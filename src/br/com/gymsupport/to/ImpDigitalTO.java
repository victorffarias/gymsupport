package br.com.gymsupport.to;

public class ImpDigitalTO extends GSAbstractTO {
	private Integer idigid;

	private String idigflgdedo;

	private String idigflgmao;

	private String idigstatus;

	private ArquivoTO arquivo;

	private PessoaTO pessoa;

	public Integer getIdigid() {
		return idigid;
	}

	public void setIdigid(Integer idigid) {
		this.idigid = idigid;
	}

	public String getIdigflgdedo() {
		return idigflgdedo;
	}

	public void setIdigflgdedo(String idigflgdedo) {
		this.idigflgdedo = idigflgdedo;
	}

	public String getIdigflgmao() {
		return idigflgmao;
	}

	public void setIdigflgmao(String idigflgmao) {
		this.idigflgmao = idigflgmao;
	}

	public String getIdigstatus() {
		return idigstatus;
	}

	public void setIdigstatus(String idigstatus) {
		this.idigstatus = idigstatus;
	}

	public ArquivoTO getArquivo() {
		return arquivo;
	}

	public void setArquivo(ArquivoTO arquivo) {
		this.arquivo = arquivo;
	}

	public PessoaTO getPessoa() {
		return pessoa;
	}

	public void setPessoa(PessoaTO pessoa) {
		this.pessoa = pessoa;
	}
	
	
}
