package br.com.gymsupport.to.factory;

import br.com.arquitetura.to.AbstractTOFile;
import br.com.arquitetura.to.factory.TOFactory;
import br.com.gymsupport.to.AlunoTO;
import br.com.gymsupport.to.PessoaTO;
import br.com.gymsupport.to.TreinoTO;

public class GSTOFactory extends TOFactory {
	static GSTOFactory instance = null;
	
	public static GSTOFactory getInstance(){
		if (instance == null) {
			instance = new GSTOFactory();
		}
		return instance;
	}
	
	public AlunoTO createAlunoTO(){
		return new AlunoTO();
	}
	
	public TreinoTO createTreinoTO(){
		return new TreinoTO();
	}
	
	public PessoaTO createPessoaTO(){
		return new PessoaTO();
	}
	
	public AbstractTOFile createAbstractTOFile(){
		return new AbstractTOFile();
	}
}
