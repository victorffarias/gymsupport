package br.com.gymsupport.to;


public class TelefoneTO extends GSAbstractTO {
	
	private Integer telid;

	private String telnumero;

	private Boolean telpreferencial;

	private String telstatus;

	private Integer teltipo;

	private PessoaTO pessoa;

	public Integer getTelid() {
		return telid;
	}

	public void setTelid(Integer telid) {
		this.telid = telid;
	}

	public String getTelnumero() {
		return telnumero;
	}

	public void setTelnumero(String telnumero) {
		this.telnumero = telnumero;
	}

	public Boolean getTelpreferencial() {
		return telpreferencial;
	}

	public void setTelpreferencial(Boolean telpreferencial) {
		this.telpreferencial = telpreferencial;
	}

	public String getTelstatus() {
		return telstatus;
	}

	public void setTelstatus(String telstatus) {
		this.telstatus = telstatus;
	}

	public Integer getTeltipo() {
		return teltipo;
	}

	public void setTeltipo(Integer teltipo) {
		this.teltipo = teltipo;
	}

	public PessoaTO getPessoa() {
		return pessoa;
	}

	public void setPessoa(PessoaTO pessoa) {
		this.pessoa = pessoa;
	}

}
