package br.com.gymsupport.to;

import java.util.List;

import br.com.arquitetura.to.AbstractTOFile;

public class PessoaTO extends GSAbstractTO {
	private Integer pesid;

	private String pesemail;

	private String pesnome;

	private String pesstatus;

	private List<AlunoTO> alunos;

	private List<EnderecoTO> enderecos;

	private List<ImpDigitalTO> impdigitals;

	private List<InstrutorTO> instrutors;

	private AbstractTOFile arquivo;

	private List<TelefoneTO> telefones;

	public Integer getPesid() {
		return pesid;
	}

	public void setPesid(Integer pesid) {
		this.pesid = pesid;
	}

	public String getPesemail() {
		return pesemail;
	}

	public void setPesemail(String pesemail) {
		this.pesemail = pesemail;
	}

	public String getPesnome() {
		return pesnome;
	}

	public void setPesnome(String pesnome) {
		this.pesnome = pesnome;
	}

	public String getPesstatus() {
		return pesstatus;
	}

	public void setPesstatus(String pesstatus) {
		this.pesstatus = pesstatus;
	}

	public List<AlunoTO> getAlunos() {
		return alunos;
	}

	public void setAlunos(List<AlunoTO> alunos) {
		this.alunos = alunos;
	}

	public List<EnderecoTO> getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(List<EnderecoTO> enderecos) {
		this.enderecos = enderecos;
	}

	public List<ImpDigitalTO> getImpdigitals() {
		return impdigitals;
	}

	public void setImpdigitals(List<ImpDigitalTO> impdigitals) {
		this.impdigitals = impdigitals;
	}

	public List<InstrutorTO> getInstrutors() {
		return instrutors;
	}

	public void setInstrutors(List<InstrutorTO> instrutors) {
		this.instrutors = instrutors;
	}

	public AbstractTOFile getArquivo() {
		return arquivo;
	}

	public void setArquivo(AbstractTOFile arquivo) {
		this.arquivo = arquivo;
	}

	public List<TelefoneTO> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<TelefoneTO> telefones) {
		this.telefones = telefones;
	}
	
}
