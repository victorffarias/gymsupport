package br.com.gymsupport.to;

import java.sql.Timestamp;
import java.util.List;

public class ArquivoTO extends GSAbstractTO {
	private Integer arqid;

	private Timestamp arqdata;

	private String arqextensao;

	private String arqnome;

	private Boolean arqstatus;

	private Long arqtamanho;

	private List<ImpDigitalTO> impdigitals;

	private List<PessoaTO> pessoas;

	public Integer getArqid() {
		return arqid;
	}

	public void setArqid(Integer arqid) {
		this.arqid = arqid;
	}

	public Timestamp getArqdata() {
		return arqdata;
	}

	public void setArqdata(Timestamp arqdata) {
		this.arqdata = arqdata;
	}

	public String getArqextensao() {
		return arqextensao;
	}

	public void setArqextensao(String arqextensao) {
		this.arqextensao = arqextensao;
	}

	public String getArqnome() {
		return arqnome;
	}

	public void setArqnome(String arqnome) {
		this.arqnome = arqnome;
	}

	public Boolean getArqstatus() {
		return arqstatus;
	}

	public void setArqstatus(Boolean arqstatus) {
		this.arqstatus = arqstatus;
	}

	public Long getArqtamanho() {
		return arqtamanho;
	}

	public void setArqtamanho(Long arqtamanho) {
		this.arqtamanho = arqtamanho;
	}

	public List<ImpDigitalTO> getImpdigitals() {
		return impdigitals;
	}

	public void setImpdigitals(List<ImpDigitalTO> impdigitals) {
		this.impdigitals = impdigitals;
	}

	public List<PessoaTO> getPessoas() {
		return pessoas;
	}

	public void setPessoas(List<PessoaTO> pessoas) {
		this.pessoas = pessoas;
	}
	
	
}
