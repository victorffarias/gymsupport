package br.com.gymsupport.to;

import java.util.List;

public class ExercicioTO extends GSAbstractTO {
	
	private Integer exeid;

	private String exenome;

	private List<EquipamentoExercicioTO> equipamentoexercicios;

	private List<ExercicioTreinoTO> exerciciotreinos;

	public Integer getExeid() {
		return exeid;
	}

	public void setExeid(Integer exeid) {
		this.exeid = exeid;
	}

	public String getExenome() {
		return exenome;
	}

	public void setExenome(String exenome) {
		this.exenome = exenome;
	}

	public List<EquipamentoExercicioTO> getEquipamentoexercicios() {
		return equipamentoexercicios;
	}

	public void setEquipamentoexercicios(
			List<EquipamentoExercicioTO> equipamentoexercicios) {
		this.equipamentoexercicios = equipamentoexercicios;
	}

	public List<ExercicioTreinoTO> getExerciciotreinos() {
		return exerciciotreinos;
	}

	public void setExerciciotreinos(List<ExercicioTreinoTO> exerciciotreinos) {
		this.exerciciotreinos = exerciciotreinos;
	}

}
