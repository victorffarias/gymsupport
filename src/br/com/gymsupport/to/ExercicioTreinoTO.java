package br.com.gymsupport.to;


public class ExercicioTreinoTO extends GSAbstractTO {
	private Integer exetid;

	private Integer exetdescanso;

	private Integer exetdescansobreakset;

	private Integer exetfatorbreakset;

	private Integer exetfatordropset;

	private Boolean exetflgbreakset;

	private Boolean exetflgdropset;

	private Integer exetnumerorep;

	private String exetobservacao;

	private String exetstatus;

	private ExercicioTO exercicio;

	private TreinoTO treino;

	public Integer getExetid() {
		return exetid;
	}

	public void setExetid(Integer exetid) {
		this.exetid = exetid;
	}

	public Integer getExetdescanso() {
		return exetdescanso;
	}

	public void setExetdescanso(Integer exetdescanso) {
		this.exetdescanso = exetdescanso;
	}

	public Integer getExetdescansobreakset() {
		return exetdescansobreakset;
	}

	public void setExetdescansobreakset(Integer exetdescansobreakset) {
		this.exetdescansobreakset = exetdescansobreakset;
	}

	public Integer getExetfatorbreakset() {
		return exetfatorbreakset;
	}

	public void setExetfatorbreakset(Integer exetfatorbreakset) {
		this.exetfatorbreakset = exetfatorbreakset;
	}

	public Integer getExetfatordropset() {
		return exetfatordropset;
	}

	public void setExetfatordropset(Integer exetfatordropset) {
		this.exetfatordropset = exetfatordropset;
	}

	public Boolean getExetflgbreakset() {
		return exetflgbreakset;
	}

	public void setExetflgbreakset(Boolean exetflgbreakset) {
		this.exetflgbreakset = exetflgbreakset;
	}

	public Boolean getExetflgdropset() {
		return exetflgdropset;
	}

	public void setExetflgdropset(Boolean exetflgdropset) {
		this.exetflgdropset = exetflgdropset;
	}

	public Integer getExetnumerorep() {
		return exetnumerorep;
	}

	public void setExetnumerorep(Integer exetnumerorep) {
		this.exetnumerorep = exetnumerorep;
	}

	public String getExetobservacao() {
		return exetobservacao;
	}

	public void setExetobservacao(String exetobservacao) {
		this.exetobservacao = exetobservacao;
	}

	public String getExetstatus() {
		return exetstatus;
	}

	public void setExetstatus(String exetstatus) {
		this.exetstatus = exetstatus;
	}

	public ExercicioTO getExercicio() {
		return exercicio;
	}

	public void setExercicio(ExercicioTO exercicio) {
		this.exercicio = exercicio;
	}

	public TreinoTO getTreino() {
		return treino;
	}

	public void setTreino(TreinoTO treino) {
		this.treino = treino;
	}

}
