package br.com.gymsupport.to;


public class AvaliacaoFisicaTO extends GSAbstractTO {
	private Integer avfid;

	private String avfdescricao;

	private String avfobjetivo;

	private String avfresultado;

	private AlunoTO aluno;

	public Integer getAvfid() {
		return avfid;
	}

	public void setAvfid(Integer avfid) {
		this.avfid = avfid;
	}

	public String getAvfdescricao() {
		return avfdescricao;
	}

	public void setAvfdescricao(String avfdescricao) {
		this.avfdescricao = avfdescricao;
	}

	public String getAvfobjetivo() {
		return avfobjetivo;
	}

	public void setAvfobjetivo(String avfobjetivo) {
		this.avfobjetivo = avfobjetivo;
	}

	public String getAvfresultado() {
		return avfresultado;
	}

	public void setAvfresultado(String avfresultado) {
		this.avfresultado = avfresultado;
	}

	public AlunoTO getAluno() {
		return aluno;
	}

	public void setAluno(AlunoTO aluno) {
		this.aluno = aluno;
	}
	
	
}
