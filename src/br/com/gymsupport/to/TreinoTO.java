package br.com.gymsupport.to;

import java.util.Date;
import java.util.List;

import br.com.gymsupport.entidades.AlunoEntity;
import br.com.gymsupport.entidades.InstrutorEntity;

public class TreinoTO extends GSAbstractTO {
	private Integer treid;

	private Date treidata;

	private Integer treiduracao;

	private String treiobjetivo;

	private String trestatus;

	private List<ExercicioTreinoTO> exerciciotreinos;

	private AlunoTO aluno;

	private InstrutorTO instrutor;

	public Integer getTreid() {
		return treid;
	}

	public void setTreid(Integer treid) {
		this.treid = treid;
	}

	public Date getTreidata() {
		return treidata;
	}

	public void setTreidata(Date treidata) {
		this.treidata = treidata;
	}

	public Integer getTreiduracao() {
		return treiduracao;
	}

	public void setTreiduracao(Integer treiduracao) {
		this.treiduracao = treiduracao;
	}

	public String getTreiobjetivo() {
		return treiobjetivo;
	}

	public void setTreiobjetivo(String treiobjetivo) {
		this.treiobjetivo = treiobjetivo;
	}

	public String getTrestatus() {
		return trestatus;
	}

	public void setTrestatus(String trestatus) {
		this.trestatus = trestatus;
	}

	public List<ExercicioTreinoTO> getExerciciotreinos() {
		return exerciciotreinos;
	}

	public void setExerciciotreinos(List<ExercicioTreinoTO> exerciciotreinos) {
		this.exerciciotreinos = exerciciotreinos;
	}

	public AlunoTO getAluno() {
		return aluno;
	}

	public void setAluno(AlunoTO aluno) {
		this.aluno = aluno;
	}

	public InstrutorTO getInstrutor() {
		return instrutor;
	}

	public void setInstrutor(InstrutorTO instrutor) {
		this.instrutor = instrutor;
	}
	
	
	
}
