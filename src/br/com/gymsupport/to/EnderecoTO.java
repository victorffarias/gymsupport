package br.com.gymsupport.to;

import br.com.gymsupport.entidades.UfEntity;

public class EnderecoTO extends GSAbstractTO {

	private Integer endid;

	private String endcep;

	private String endcomplemento;

	private String enddescricao;

	private String endnumero;

	private String endstatus;

	private CidadeTO cidade;

	private PessoaTO pessoa;

	private UfEntity uf;

	public Integer getEndid() {
		return endid;
	}

	public void setEndid(Integer endid) {
		this.endid = endid;
	}

	public String getEndcep() {
		return endcep;
	}

	public void setEndcep(String endcep) {
		this.endcep = endcep;
	}

	public String getEndcomplemento() {
		return endcomplemento;
	}

	public void setEndcomplemento(String endcomplemento) {
		this.endcomplemento = endcomplemento;
	}

	public String getEnddescricao() {
		return enddescricao;
	}

	public void setEnddescricao(String enddescricao) {
		this.enddescricao = enddescricao;
	}

	public String getEndnumero() {
		return endnumero;
	}

	public void setEndnumero(String endnumero) {
		this.endnumero = endnumero;
	}

	public String getEndstatus() {
		return endstatus;
	}

	public void setEndstatus(String endstatus) {
		this.endstatus = endstatus;
	}

	public CidadeTO getCidade() {
		return cidade;
	}

	public void setCidade(CidadeTO cidade) {
		this.cidade = cidade;
	}

	public PessoaTO getPessoa() {
		return pessoa;
	}

	public void setPessoa(PessoaTO pessoa) {
		this.pessoa = pessoa;
	}

	public UfEntity getUf() {
		return uf;
	}

	public void setUf(UfEntity uf) {
		this.uf = uf;
	}
}
