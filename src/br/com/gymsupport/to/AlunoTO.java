package br.com.gymsupport.to;

import java.util.List;

public class AlunoTO extends GSAbstractTO {
	private Integer aluid;

	private String alustatus;

	private PessoaTO pessoa;

	private List<AvaliacaoFisicaTO> avaliacaofisicas;

	private List<TreinoTO> treinos;

	public Integer getAluid() {
		return this.aluid;
	}

	public void setAluid(Integer aluid) {
		this.aluid = aluid;
	}

	public String getAlustatus() {
		return this.alustatus;
	}

	public void setAlustatus(String alustatus) {
		this.alustatus = alustatus;
	}

	public PessoaTO getPessoa() {
		return this.pessoa;
	}

	public void setPessoa(PessoaTO pessoa) {
		this.pessoa = pessoa;
	}

	public List<AvaliacaoFisicaTO> getAvaliacaofisicas() {
		return this.avaliacaofisicas;
	}

	public void setAvaliacaofisicas(List<AvaliacaoFisicaTO> avaliacaofisicas) {
		this.avaliacaofisicas = avaliacaofisicas;
	}

	public List<TreinoTO> getTreinos() {
		return this.treinos;
	}

	public void setTreinos(List<TreinoTO> treinos) {
		this.treinos = treinos;
	}	
	
}
