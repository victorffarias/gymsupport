package br.com.gymsupport.to;

import java.util.List;

public class InstrutorTO extends GSAbstractTO {
	private Integer instid;

	private String inststatus;

	private List<EscalaTO> escalas;

	private PessoaTO pessoa;

	private List<TreinoTO> treinos;

	public Integer getInstid() {
		return instid;
	}

	public void setInstid(Integer instid) {
		this.instid = instid;
	}

	public String getInststatus() {
		return inststatus;
	}

	public void setInststatus(String inststatus) {
		this.inststatus = inststatus;
	}

	public List<EscalaTO> getEscalas() {
		return escalas;
	}

	public void setEscalas(List<EscalaTO> escalas) {
		this.escalas = escalas;
	}

	public PessoaTO getPessoa() {
		return pessoa;
	}

	public void setPessoa(PessoaTO pessoa) {
		this.pessoa = pessoa;
	}

	public List<TreinoTO> getTreinos() {
		return treinos;
	}

	public void setTreinos(List<TreinoTO> treinos) {
		this.treinos = treinos;
	}
	
}
