package br.com.gymsupport.entidades;

import java.io.Serializable;
import java.sql.Time;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the periodo database table.
 * 
 */
@Entity
@Table(name="gymsupport.periodo")
@NamedQuery(name="PeriodoEntity.findAll", query="SELECT p FROM PeriodoEntity p")
public class PeriodoEntity extends GSAbstractEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer perid;

	private Boolean perflgestagiario;

	private Time perhorinicio;

	private Time perhortermino;

	//bi-directional many-to-one association to EscalaDiaEntity
	@OneToMany(mappedBy="periodo")
	private List<EscalaDiaEntity> escaladias;

	public PeriodoEntity() {
	}

	public Integer getPerid() {
		return this.perid;
	}

	public void setPerid(Integer perid) {
		this.perid = perid;
	}

	public Boolean getPerflgestagiario() {
		return this.perflgestagiario;
	}

	public void setPerflgestagiario(Boolean perflgestagiario) {
		this.perflgestagiario = perflgestagiario;
	}

	public Time getPerhorinicio() {
		return this.perhorinicio;
	}

	public void setPerhorinicio(Time perhorinicio) {
		this.perhorinicio = perhorinicio;
	}

	public Time getPerhortermino() {
		return this.perhortermino;
	}

	public void setPerhortermino(Time perhortermino) {
		this.perhortermino = perhortermino;
	}

	public List<EscalaDiaEntity> getEscaladias() {
		return this.escaladias;
	}

	public void setEscaladias(List<EscalaDiaEntity> escaladias) {
		this.escaladias = escaladias;
	}

	public EscalaDiaEntity addEscaladia(EscalaDiaEntity escaladia) {
		getEscaladias().add(escaladia);
		escaladia.setPeriodo(this);

		return escaladia;
	}

	public EscalaDiaEntity removeEscaladia(EscalaDiaEntity escaladia) {
		getEscaladias().remove(escaladia);
		escaladia.setPeriodo(null);

		return escaladia;
	}

	@Override
	protected Integer doGetId() {
		return this.perid;
	}

}