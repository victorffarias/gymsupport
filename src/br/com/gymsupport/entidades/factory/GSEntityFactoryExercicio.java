package br.com.gymsupport.entidades.factory;

import java.util.HashMap;

import br.com.arquitetura.entidades.factory.EntityFactory;
import br.com.gymsupport.entidades.AlunoEntity;
import br.com.gymsupport.entidades.ArquivoEntity;
import br.com.gymsupport.entidades.GSAbstractEntity;

public class GSEntityFactoryExercicio<T extends GSAbstractEntity> extends EntityFactory {
	
	private HashMap<GSEntityEnum, T> entities = new HashMap<GSEntityEnum, T>();

	@SuppressWarnings("unchecked")
	public T get(GSEntityEnum key){
		entities.put(GSEntityEnum.aluno, (T) new AlunoEntity());
		entities.put(GSEntityEnum.arquivo, (T) new ArquivoEntity());
		return entities.get(key);
	}
	
	public static void main(String[] args) {
		GSEntityFactoryExercicio<AlunoEntity> factoryAluno = new GSEntityFactoryExercicio<AlunoEntity>();
		AlunoEntity aluno = factoryAluno.get(GSEntityEnum.aluno);
		System.out.println(aluno);
		
		GSEntityFactoryExercicio<ArquivoEntity> factoryArquivo = new GSEntityFactoryExercicio<ArquivoEntity>();
		ArquivoEntity arquivo = factoryArquivo.get(GSEntityEnum.arquivo);
		System.out.println(arquivo);
	}
}
