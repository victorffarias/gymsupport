package br.com.gymsupport.entidades.factory;

import br.com.arquitetura.entidades.factory.EntityFactory;
import br.com.gymsupport.entidades.AlunoEntity;
import br.com.gymsupport.entidades.AvaliacaoFisicaEntity;
import br.com.gymsupport.entidades.PessoaEntity;
import br.com.gymsupport.entidades.TreinoEntity;

public class GSEntityFactory extends EntityFactory {
	static GSEntityFactory instance = null;
	
	public static GSEntityFactory getInstance(){
		if (instance == null) {
			instance = new GSEntityFactory();
		}
		
		return instance;
	}
	
	public AlunoEntity createAlunoEntity(){
		return new AlunoEntity();
	}
	
	public TreinoEntity createTreinoEntity(){
		return new TreinoEntity();
	}
	
	public PessoaEntity createPessoaEntity(){
		return new PessoaEntity();
	}
	
	public AvaliacaoFisicaEntity createAvaliacaoFisicaEntity(){
		return new AvaliacaoFisicaEntity();
	}
}
