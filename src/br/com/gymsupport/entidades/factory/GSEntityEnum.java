package br.com.gymsupport.entidades.factory;

public enum GSEntityEnum {
	aluno(1), arquivo(2);
	
	private int valor;
	
	private GSEntityEnum(int valor) {
		this.setValor(valor);
	}

	public int getValor() {
		return valor;
	}

	public void setValor(int valor) {
		this.valor = valor;
	}
	
	
}
