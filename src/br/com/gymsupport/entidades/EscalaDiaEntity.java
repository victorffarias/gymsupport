package br.com.gymsupport.entidades;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the escaladia database table.
 * 
 */
@Entity
@Table(name="gymsupport.escaladia")
@NamedQuery(name="EscalaDiaEntity.findAll", query="SELECT e FROM EscalaDiaEntity e")
public class EscalaDiaEntity extends GSAbstractEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer escdid;

	private String escddia;

	//bi-directional many-to-one association to EscalaEntity
	@ManyToOne
	@JoinColumn(name="escid")
	private EscalaEntity escala;

	//bi-directional many-to-one association to PeriodoEntity
	@ManyToOne
	@JoinColumn(name="perid")
	private PeriodoEntity periodo;

	public EscalaDiaEntity() {
	}

	public Integer getEscdid() {
		return this.escdid;
	}

	public void setEscdid(Integer escdid) {
		this.escdid = escdid;
	}

	public String getEscddia() {
		return this.escddia;
	}

	public void setEscddia(String escddia) {
		this.escddia = escddia;
	}

	public EscalaEntity getEscala() {
		return this.escala;
	}

	public void setEscala(EscalaEntity escala) {
		this.escala = escala;
	}

	public PeriodoEntity getPeriodo() {
		return this.periodo;
	}

	public void setPeriodo(PeriodoEntity periodo) {
		this.periodo = periodo;
	}

	@Override
	protected Integer doGetId() {
		return this.escdid;
	}

}