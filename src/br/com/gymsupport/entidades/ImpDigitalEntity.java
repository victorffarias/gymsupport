package br.com.gymsupport.entidades;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * The persistent class for the impdigital database table.
 * 
 */
@Entity
@Table(name="gymsupport.impdigital")
@NamedQuery(name="ImpDigitalEntity.findAll", query="SELECT i FROM ImpDigitalEntity i")
public class ImpDigitalEntity extends GSAbstractEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer idigid;

	private Integer arqid;

	@Column(name="flg_dedo")
	private String flgDedo;

	@Column(name="flg_mao")
	private String flgMao;

	//bi-directional many-to-one association to PessoaEntity
	@ManyToOne
	@JoinColumn(name="pesid")
	private PessoaEntity pessoa;

	//bi-directional one-to-one association to ArquivoEntity
	@OneToOne(mappedBy="impdigital")
	private ArquivoEntity arquivo;

	public ImpDigitalEntity() {
	}

	public Integer getIdigid() {
		return this.idigid;
	}

	public void setIdigid(Integer idigid) {
		this.idigid = idigid;
	}

	public Integer getArqid() {
		return this.arqid;
	}

	public void setArqid(Integer arqid) {
		this.arqid = arqid;
	}

	public String getFlgDedo() {
		return this.flgDedo;
	}

	public void setFlgDedo(String flgDedo) {
		this.flgDedo = flgDedo;
	}

	public String getFlgMao() {
		return this.flgMao;
	}

	public void setFlgMao(String flgMao) {
		this.flgMao = flgMao;
	}

	public PessoaEntity getPessoa() {
		return this.pessoa;
	}

	public void setPessoa(PessoaEntity pessoa) {
		this.pessoa = pessoa;
	}

	public ArquivoEntity getArquivo() {
		return this.arquivo;
	}

	public void setArquivo(ArquivoEntity arquivo) {
		this.arquivo = arquivo;
	}

	@Override
	protected Integer doGetId() {
		return this.idigid;
	}

}