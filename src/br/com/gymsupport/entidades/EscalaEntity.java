package br.com.gymsupport.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the escala database table.
 * 
 */
@Entity
@Table(name="gymsupport.escala")
@NamedQuery(name="EscalaEntity.findAll", query="SELECT e FROM EscalaEntity e")
public class EscalaEntity extends GSAbstractEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer escid;

	@Temporal(TemporalType.DATE)
	private Date escdatinicioescala;

	@Temporal(TemporalType.DATE)
	private Date escdatterminoescala;

	private String escstatus;

	//bi-directional many-to-one association to EscalaDiaEntity
	@OneToMany(mappedBy="escala")
	private List<EscalaDiaEntity> escaladias;

	//bi-directional many-to-one association to InstrutorEntity
	@OneToMany(mappedBy="escala")
	private List<InstrutorEntity> instrutors;

	public EscalaEntity() {
	}

	public Integer getEscid() {
		return this.escid;
	}

	public void setEscid(Integer escid) {
		this.escid = escid;
	}

	public Date getEscdatinicioescala() {
		return this.escdatinicioescala;
	}

	public void setEscdatinicioescala(Date escdatinicioescala) {
		this.escdatinicioescala = escdatinicioescala;
	}

	public Date getEscdatterminoescala() {
		return this.escdatterminoescala;
	}

	public void setEscdatterminoescala(Date escdatterminoescala) {
		this.escdatterminoescala = escdatterminoescala;
	}

	public String getEscstatus() {
		return this.escstatus;
	}

	public void setEscstatus(String escstatus) {
		this.escstatus = escstatus;
	}

	public List<EscalaDiaEntity> getEscaladias() {
		return this.escaladias;
	}

	public void setEscaladias(List<EscalaDiaEntity> escaladias) {
		this.escaladias = escaladias;
	}

	public EscalaDiaEntity addEscaladia(EscalaDiaEntity escaladia) {
		getEscaladias().add(escaladia);
		escaladia.setEscala(this);

		return escaladia;
	}

	public EscalaDiaEntity removeEscaladia(EscalaDiaEntity escaladia) {
		getEscaladias().remove(escaladia);
		escaladia.setEscala(null);

		return escaladia;
	}

	public List<InstrutorEntity> getInstrutors() {
		return this.instrutors;
	}

	public void setInstrutors(List<InstrutorEntity> instrutors) {
		this.instrutors = instrutors;
	}

	public InstrutorEntity addInstrutor(InstrutorEntity instrutor) {
		getInstrutors().add(instrutor);
		instrutor.setEscala(this);

		return instrutor;
	}

	public InstrutorEntity removeInstrutor(InstrutorEntity instrutor) {
		getInstrutors().remove(instrutor);
		instrutor.setEscala(null);

		return instrutor;
	}

	@Override
	protected Integer doGetId() {
		return this.escid;
	}

}