package br.com.gymsupport.entidades;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the telefone database table.
 * 
 */
@Entity
@Table(name="gymsupport.telefone")
@NamedQuery(name="TelefoneEntity.findAll", query="SELECT t FROM TelefoneEntity t")
public class TelefoneEntity extends GSAbstractEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer telid;

	private String telnumero;

	private Boolean telpreferencial;

	private Integer teltipo;

	//bi-directional many-to-one association to PessoaEntity
	@ManyToOne
	@JoinColumn(name="pesid")
	private PessoaEntity pessoa;

	public TelefoneEntity() {
	}

	public Integer getTelid() {
		return this.telid;
	}

	public void setTelid(Integer telid) {
		this.telid = telid;
	}

	public String getTelnumero() {
		return this.telnumero;
	}

	public void setTelnumero(String telnumero) {
		this.telnumero = telnumero;
	}

	public Boolean getTelpreferencial() {
		return this.telpreferencial;
	}

	public void setTelpreferencial(Boolean telpreferencial) {
		this.telpreferencial = telpreferencial;
	}

	public Integer getTeltipo() {
		return this.teltipo;
	}

	public void setTeltipo(Integer teltipo) {
		this.teltipo = teltipo;
	}

	public PessoaEntity getPessoa() {
		return this.pessoa;
	}

	public void setPessoa(PessoaEntity pessoa) {
		this.pessoa = pessoa;
	}

	@Override
	protected Integer doGetId() {
		return this.telid;
	}

}