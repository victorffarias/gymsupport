package br.com.gymsupport.entidades;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the equipamentoexercicio database table.
 * 
 */
@Entity
@Table(name="gymsupport.equipamentoexercicio")
@NamedQuery(name="EquipamentoExercicioEntity.findAll", query="SELECT e FROM EquipamentoExercicioEntity e")
public class EquipamentoExercicioEntity extends GSAbstractEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer eqeid;

	//bi-directional many-to-one association to EquipamentoEntity
	@ManyToOne
	@JoinColumn(name="eqpid")
	private EquipamentoEntity equipamento;

	//bi-directional many-to-one association to ExercicioEntity
	@ManyToOne
	@JoinColumn(name="exeid")
	private ExercicioEntity exercicio;

	public EquipamentoExercicioEntity() {
	}

	public Integer getEqeid() {
		return this.eqeid;
	}

	public void setEqeid(Integer eqeid) {
		this.eqeid = eqeid;
	}

	public EquipamentoEntity getEquipamento() {
		return this.equipamento;
	}

	public void setEquipamento(EquipamentoEntity equipamento) {
		this.equipamento = equipamento;
	}

	public ExercicioEntity getExercicio() {
		return this.exercicio;
	}

	public void setExercicio(ExercicioEntity exercicio) {
		this.exercicio = exercicio;
	}

	@Override
	protected Integer doGetId() {
		return this.eqeid;
	}

}