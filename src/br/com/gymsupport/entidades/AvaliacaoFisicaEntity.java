package br.com.gymsupport.entidades;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * The persistent class for the avaliacaofisica database table.
 * 
 */
@Entity
@Table(name="gymsupport.avaliacaofisica")
@NamedQuery(name="AvaliacaoFisicaEntity.findAll", query="SELECT a FROM AvaliacaoFisicaEntity a")
public class AvaliacaoFisicaEntity extends GSAbstractEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer avfid;

	//bi-directional many-to-one association to AlunoEntity
	@ManyToOne
	@JoinColumn(name="aluid")
	private AlunoEntity aluno;

	//bi-directional one-to-one association to DadoAvaliacaoFisicaEntity
	@OneToOne(mappedBy="avaliacaofisica")
	private DadoAvaliacaoFisicaEntity dadoavaliacaofisica;

	public AvaliacaoFisicaEntity() {
	}

	public Integer getAvfid() {
		return this.avfid;
	}

	public void setAvfid(Integer avfid) {
		this.avfid = avfid;
	}

	public AlunoEntity getAluno() {
		return this.aluno;
	}

	public void setAluno(AlunoEntity aluno) {
		this.aluno = aluno;
	}

	public DadoAvaliacaoFisicaEntity getDadoavaliacaofisica() {
		return this.dadoavaliacaofisica;
	}

	public void setDadoavaliacaofisica(DadoAvaliacaoFisicaEntity dadoavaliacaofisica) {
		this.dadoavaliacaofisica = dadoavaliacaofisica;
	}

	@Override
	protected Integer doGetId() {
		return this.avfid;
	}

}