package br.com.gymsupport.entidades;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the equipamento database table.
 * 
 */
@Entity
@Table(name="gymsupport.equipamento")
@NamedQuery(name="EquipamentoEntity.findAll", query="SELECT e FROM EquipamentoEntity e")
public class EquipamentoEntity extends GSAbstractEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer eqpid;

	private String eqpnome;

	private Integer eqpnumero;

	//bi-directional many-to-one association to EquipamentoExercicioEntity
	@OneToMany(mappedBy="equipamento")
	private List<EquipamentoExercicioEntity> equipamentoexercicios;

	public EquipamentoEntity() {
	}

	public Integer getEqpid() {
		return this.eqpid;
	}

	public void setEqpid(Integer eqpid) {
		this.eqpid = eqpid;
	}

	public String getEqpnome() {
		return this.eqpnome;
	}

	public void setEqpnome(String eqpnome) {
		this.eqpnome = eqpnome;
	}

	public Integer getEqpnumero() {
		return this.eqpnumero;
	}

	public void setEqpnumero(Integer eqpnumero) {
		this.eqpnumero = eqpnumero;
	}

	public List<EquipamentoExercicioEntity> getEquipamentoexercicios() {
		return this.equipamentoexercicios;
	}

	public void setEquipamentoexercicios(List<EquipamentoExercicioEntity> equipamentoexercicios) {
		this.equipamentoexercicios = equipamentoexercicios;
	}

	public EquipamentoExercicioEntity addEquipamentoexercicio(EquipamentoExercicioEntity equipamentoexercicio) {
		getEquipamentoexercicios().add(equipamentoexercicio);
		equipamentoexercicio.setEquipamento(this);

		return equipamentoexercicio;
	}

	public EquipamentoExercicioEntity removeEquipamentoexercicio(EquipamentoExercicioEntity equipamentoexercicio) {
		getEquipamentoexercicios().remove(equipamentoexercicio);
		equipamentoexercicio.setEquipamento(null);

		return equipamentoexercicio;
	}

	@Override
	protected Integer doGetId() {
		return this.eqpid;
	}

}