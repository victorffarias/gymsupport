package br.com.gymsupport.entidades;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * The persistent class for the endereco database table.
 * 
 */
@Entity
@Table(name="gymsupport.endereco")
@NamedQuery(name="EnderecoEntity.findAll", query="SELECT e FROM EnderecoEntity e")
public class EnderecoEntity extends GSAbstractEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer endid;

	private Integer cidid;

	private String endcep;

	private String endcomplemento;

	private String enddescricao;

	private String endnumero;

	private Integer ufid;

	//bi-directional one-to-one association to CidadeEntity
	@OneToOne
	@JoinColumn(name="endid")
	private CidadeEntity cidade;

	//bi-directional one-to-one association to UfEntity
	@OneToOne
	@JoinColumn(name="endid")
	private UfEntity uf;

	//bi-directional one-to-one association to PessoaEntity
	@OneToOne(mappedBy="endereco")
	private PessoaEntity pessoa;

	public EnderecoEntity() {
	}

	public Integer getEndid() {
		return this.endid;
	}

	public void setEndid(Integer endid) {
		this.endid = endid;
	}

	public Integer getCidid() {
		return this.cidid;
	}

	public void setCidid(Integer cidid) {
		this.cidid = cidid;
	}

	public String getEndcep() {
		return this.endcep;
	}

	public void setEndcep(String endcep) {
		this.endcep = endcep;
	}

	public String getEndcomplemento() {
		return this.endcomplemento;
	}

	public void setEndcomplemento(String endcomplemento) {
		this.endcomplemento = endcomplemento;
	}

	public String getEnddescricao() {
		return this.enddescricao;
	}

	public void setEnddescricao(String enddescricao) {
		this.enddescricao = enddescricao;
	}

	public String getEndnumero() {
		return this.endnumero;
	}

	public void setEndnumero(String endnumero) {
		this.endnumero = endnumero;
	}

	public Integer getUfid() {
		return this.ufid;
	}

	public void setUfid(Integer ufid) {
		this.ufid = ufid;
	}

	public CidadeEntity getCidade() {
		return this.cidade;
	}

	public void setCidade(CidadeEntity cidade) {
		this.cidade = cidade;
	}

	public UfEntity getUf() {
		return this.uf;
	}

	public void setUf(UfEntity uf) {
		this.uf = uf;
	}

	public PessoaEntity getPessoa() {
		return this.pessoa;
	}

	public void setPessoa(PessoaEntity pessoa) {
		this.pessoa = pessoa;
	}

	@Override
	protected Integer doGetId() {
		return this.endid;
	}

}