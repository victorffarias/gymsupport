package br.com.gymsupport.entidades;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * The persistent class for the arquivo database table.
 * 
 */
@Entity
@Table(name="gymsupport.arquivo")
@NamedQuery(name="ArquivoEntity.findAll", query="SELECT a FROM ArquivoEntity a")
public class ArquivoEntity extends GSAbstractEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer arqid;

	private Timestamp arqdata;

	private String arqextensao;

	private String arqnome;

	private Boolean arqstatus;

	private Long arqtamanho;

	//bi-directional one-to-one association to ImpDigitalEntity
	@OneToOne
	@JoinColumn(name="arqid")
	private ImpDigitalEntity impdigital;

	//bi-directional one-to-one association to PessoaEntity
	@OneToOne(mappedBy="arquivo")
	private PessoaEntity pessoa;

	public ArquivoEntity() {
	}

	public Integer getArqid() {
		return this.arqid;
	}

	public void setArqid(Integer arqid) {
		this.arqid = arqid;
	}

	public Timestamp getArqdata() {
		return this.arqdata;
	}

	public void setArqdata(Timestamp arqdata) {
		this.arqdata = arqdata;
	}

	public String getArqextensao() {
		return this.arqextensao;
	}

	public void setArqextensao(String arqextensao) {
		this.arqextensao = arqextensao;
	}

	public String getArqnome() {
		return this.arqnome;
	}

	public void setArqnome(String arqnome) {
		this.arqnome = arqnome;
	}

	public Boolean getArqstatus() {
		return this.arqstatus;
	}

	public void setArqstatus(Boolean arqstatus) {
		this.arqstatus = arqstatus;
	}

	public Long getArqtamanho() {
		return this.arqtamanho;
	}

	public void setArqtamanho(Long arqtamanho) {
		this.arqtamanho = arqtamanho;
	}

	public ImpDigitalEntity getImpdigital() {
		return this.impdigital;
	}

	public void setImpdigital(ImpDigitalEntity impdigital) {
		this.impdigital = impdigital;
	}

	public PessoaEntity getPessoa() {
		return this.pessoa;
	}

	public void setPessoa(PessoaEntity pessoa) {
		this.pessoa = pessoa;
	}

	@Override
	protected Integer doGetId() {
		return this.arqid;
	}

}