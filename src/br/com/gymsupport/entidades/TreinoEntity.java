package br.com.gymsupport.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the treino database table.
 * 
 */
@Entity
@Table(name="gymsupport.treino")
@NamedQuery(name="TreinoEntity.findAll", query="SELECT t FROM TreinoEntity t")
public class TreinoEntity extends GSAbstractEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer treid;

	@Temporal(TemporalType.DATE)
	private Date treidata;

	private Integer treiduracao;

	private String treiobjetivo;

	//bi-directional many-to-one association to ExercicioTreinoEntity
	@OneToMany(mappedBy="treino")
	private List<ExercicioTreinoEntity> exerciciotreinos;

	//bi-directional many-to-one association to AlunoEntity
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="aluid", insertable = true, updatable = true)
	private AlunoEntity aluno;

	//bi-directional many-to-one association to InstrutorEntity
	@ManyToOne
	@JoinColumn(name="instid")
	private InstrutorEntity instrutor;

	public TreinoEntity() {
	}

	public Integer getTreid() {
		return this.treid;
	}

	public void setTreid(Integer treid) {
		this.treid = treid;
	}

	public Date getTreidata() {
		return this.treidata;
	}

	public void setTreidata(Date treidata) {
		this.treidata = treidata;
	}

	public Integer getTreiduracao() {
		return this.treiduracao;
	}

	public void setTreiduracao(Integer treiduracao) {
		this.treiduracao = treiduracao;
	}

	public String getTreiobjetivo() {
		return this.treiobjetivo;
	}

	public void setTreiobjetivo(String treiobjetivo) {
		this.treiobjetivo = treiobjetivo;
	}

	public List<ExercicioTreinoEntity> getExerciciotreinos() {
		return this.exerciciotreinos;
	}

	public void setExerciciotreinos(List<ExercicioTreinoEntity> exerciciotreinos) {
		this.exerciciotreinos = exerciciotreinos;
	}

	public ExercicioTreinoEntity addExerciciotreino(ExercicioTreinoEntity exerciciotreino) {
		getExerciciotreinos().add(exerciciotreino);
		exerciciotreino.setTreino(this);

		return exerciciotreino;
	}

	public ExercicioTreinoEntity removeExerciciotreino(ExercicioTreinoEntity exerciciotreino) {
		getExerciciotreinos().remove(exerciciotreino);
		exerciciotreino.setTreino(null);

		return exerciciotreino;
	}

	public AlunoEntity getAluno() {
		return this.aluno;
	}

	public void setAluno(AlunoEntity aluno) {
		this.aluno = aluno;
	}

	public InstrutorEntity getInstrutor() {
		return this.instrutor;
	}

	public void setInstrutor(InstrutorEntity instrutor) {
		this.instrutor = instrutor;
	}

	@Override
	protected Integer doGetId() {
		return this.treid;
	}

}