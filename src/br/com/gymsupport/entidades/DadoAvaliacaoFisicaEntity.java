package br.com.gymsupport.entidades;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * The persistent class for the dadoavaliacaofisica database table.
 * 
 */
@Entity
@Table(name="gymsupport.dadoavaliacaofisica")
@NamedQuery(name="DadoAvaliacaoFisicaEntity.findAll", query="SELECT d FROM DadoAvaliacaoFisicaEntity d")
public class DadoAvaliacaoFisicaEntity extends GSAbstractEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer avfdid;

	private String avfddescricao;

	private String avfdresultado;

	private Integer avfid;

	//bi-directional one-to-one association to AvaliacaoFisicaEntity
	@OneToOne
	@JoinColumn(name="avfdid")
	private AvaliacaoFisicaEntity avaliacaofisica;

	public DadoAvaliacaoFisicaEntity() {
	}

	public Integer getAvfdid() {
		return this.avfdid;
	}

	public void setAvfdid(Integer avfdid) {
		this.avfdid = avfdid;
	}

	public String getAvfddescricao() {
		return this.avfddescricao;
	}

	public void setAvfddescricao(String avfddescricao) {
		this.avfddescricao = avfddescricao;
	}

	public String getAvfdresultado() {
		return this.avfdresultado;
	}

	public void setAvfdresultado(String avfdresultado) {
		this.avfdresultado = avfdresultado;
	}

	public Integer getAvfid() {
		return this.avfid;
	}

	public void setAvfid(Integer avfid) {
		this.avfid = avfid;
	}

	public AvaliacaoFisicaEntity getAvaliacaofisica() {
		return this.avaliacaofisica;
	}

	public void setAvaliacaofisica(AvaliacaoFisicaEntity avaliacaofisica) {
		this.avaliacaofisica = avaliacaofisica;
	}

	@Override
	protected Integer doGetId() {
		return this.avfdid;
	}

}