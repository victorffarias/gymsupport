package br.com.gymsupport.entidades;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * The persistent class for the instrutor database table.
 * 
 */
@Entity
@Table(name="gymsupport.instrutor")
@NamedQuery(name="InstrutorEntity.findAll", query="SELECT i FROM InstrutorEntity i")
public class InstrutorEntity extends GSAbstractEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer instid;

	private Integer pesid;

	//bi-directional many-to-one association to EscalaEntity
	@ManyToOne
	@JoinColumn(name="escid")
	private EscalaEntity escala;

	//bi-directional many-to-one association to TreinoEntity
	@OneToMany(mappedBy="instrutor")
	private List<TreinoEntity> treinos;

	//bi-directional one-to-one association to PessoaEntity
	@OneToOne(mappedBy="instrutor")
	private PessoaEntity pessoa;

	public InstrutorEntity() {
	}

	public Integer getInstid() {
		return this.instid;
	}

	public void setInstid(Integer instid) {
		this.instid = instid;
	}

	public Integer getPesid() {
		return this.pesid;
	}

	public void setPesid(Integer pesid) {
		this.pesid = pesid;
	}

	public EscalaEntity getEscala() {
		return this.escala;
	}

	public void setEscala(EscalaEntity escala) {
		this.escala = escala;
	}

	public List<TreinoEntity> getTreinos() {
		return this.treinos;
	}

	public void setTreinos(List<TreinoEntity> treinos) {
		this.treinos = treinos;
	}

	public TreinoEntity addTreino(TreinoEntity treino) {
		getTreinos().add(treino);
		treino.setInstrutor(this);

		return treino;
	}

	public TreinoEntity removeTreino(TreinoEntity treino) {
		getTreinos().remove(treino);
		treino.setInstrutor(null);

		return treino;
	}

	public PessoaEntity getPessoa() {
		return this.pessoa;
	}

	public void setPessoa(PessoaEntity pessoa) {
		this.pessoa = pessoa;
	}

	@Override
	protected Integer doGetId() {
		return this.instid;
	}

}