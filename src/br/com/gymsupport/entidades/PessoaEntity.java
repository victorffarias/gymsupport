package br.com.gymsupport.entidades;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.arquitetura.entidades.AbstractEntityFile;


/**
 * The persistent class for the pessoa database table.
 * 
 */
@Entity
@Table(name="gymsupport.pessoa")
@NamedQuery(name="PessoaEntity.findAll", query="SELECT p FROM PessoaEntity p")
public class PessoaEntity extends GSAbstractEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer pesid;

	private Integer arqid;

	private Integer endid;

	private String pesemail;

	private String pesnome;

	//bi-directional many-to-one association to ImpDigitalEntity
	@OneToMany(mappedBy="pessoa")
	private List<ImpDigitalEntity> impdigitals;

	//bi-directional many-to-one association to TelefoneEntity
	@OneToMany(mappedBy="pessoa")
	private List<TelefoneEntity> telefones;

	//bi-directional one-to-one association to AlunoEntity
	@OneToOne(mappedBy="pessoa")
	private AlunoEntity aluno;

	//bi-directional one-to-one association to InstrutorEntity
	@OneToOne
	@JoinColumn(name="pesid")
	private InstrutorEntity instrutor;

	//bi-directional one-to-one association to ArquivoEntity
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="pesid", insertable = true, updatable = true)
	private AbstractEntityFile arquivo;

	//bi-directional one-to-one association to EnderecoEntity
	@OneToOne
	@JoinColumn(name="pesid")
	private EnderecoEntity endereco;

	public PessoaEntity() {
	}

	public Integer getPesid() {
		return this.pesid;
	}

	public void setPesid(Integer pesid) {
		this.pesid = pesid;
	}

	public Integer getArqid() {
		return this.arqid;
	}

	public void setArqid(Integer arqid) {
		this.arqid = arqid;
	}

	public Integer getEndid() {
		return this.endid;
	}

	public void setEndid(Integer endid) {
		this.endid = endid;
	}

	public String getPesemail() {
		return this.pesemail;
	}

	public void setPesemail(String pesemail) {
		this.pesemail = pesemail;
	}

	public String getPesnome() {
		return this.pesnome;
	}

	public void setPesnome(String pesnome) {
		this.pesnome = pesnome;
	}

	public List<ImpDigitalEntity> getImpdigitals() {
		return this.impdigitals;
	}

	public void setImpdigitals(List<ImpDigitalEntity> impdigitals) {
		this.impdigitals = impdigitals;
	}

	public ImpDigitalEntity addImpdigital(ImpDigitalEntity impdigital) {
		getImpdigitals().add(impdigital);
		impdigital.setPessoa(this);

		return impdigital;
	}

	public ImpDigitalEntity removeImpdigital(ImpDigitalEntity impdigital) {
		getImpdigitals().remove(impdigital);
		impdigital.setPessoa(null);

		return impdigital;
	}

	public List<TelefoneEntity> getTelefones() {
		return this.telefones;
	}

	public void setTelefones(List<TelefoneEntity> telefones) {
		this.telefones = telefones;
	}

	public TelefoneEntity addTelefone(TelefoneEntity telefone) {
		getTelefones().add(telefone);
		telefone.setPessoa(this);

		return telefone;
	}

	public TelefoneEntity removeTelefone(TelefoneEntity telefone) {
		getTelefones().remove(telefone);
		telefone.setPessoa(null);

		return telefone;
	}

	public AlunoEntity getAluno() {
		return this.aluno;
	}

	public void setAluno(AlunoEntity aluno) {
		this.aluno = aluno;
	}

	public InstrutorEntity getInstrutor() {
		return this.instrutor;
	}

	public void setInstrutor(InstrutorEntity instrutor) {
		this.instrutor = instrutor;
	}

	public AbstractEntityFile getArquivo() {
		return this.arquivo;
	}

	public void setArquivo(AbstractEntityFile arquivo) {
		this.arquivo = arquivo;
	}

	public EnderecoEntity getEndereco() {
		return this.endereco;
	}

	public void setEndereco(EnderecoEntity endereco) {
		this.endereco = endereco;
	}

	@Override
	protected Integer doGetId() {
		return this.pesid;
	}

}