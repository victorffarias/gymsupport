package br.com.gymsupport.entidades;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the exercicio database table.
 * 
 */
@Entity
@Table(name="gymsupport.exercicio")
@NamedQuery(name="ExercicioEntity.findAll", query="SELECT e FROM ExercicioEntity e")
public class ExercicioEntity extends GSAbstractEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer exeid;

	private String exenome;

	//bi-directional many-to-one association to EquipamentoExercicioEntity
	@OneToMany(mappedBy="exercicio")
	private List<EquipamentoExercicioEntity> equipamentoexercicios;

	//bi-directional many-to-one association to ExercicioTreinoEntity
	@OneToMany(mappedBy="exercicio")
	private List<ExercicioTreinoEntity> exerciciotreinos;

	public ExercicioEntity() {
	}

	public Integer getExeid() {
		return this.exeid;
	}

	public void setExeid(Integer exeid) {
		this.exeid = exeid;
	}

	public String getExenome() {
		return this.exenome;
	}

	public void setExenome(String exenome) {
		this.exenome = exenome;
	}

	public List<EquipamentoExercicioEntity> getEquipamentoexercicios() {
		return this.equipamentoexercicios;
	}

	public void setEquipamentoexercicios(List<EquipamentoExercicioEntity> equipamentoexercicios) {
		this.equipamentoexercicios = equipamentoexercicios;
	}

	public EquipamentoExercicioEntity addEquipamentoexercicio(EquipamentoExercicioEntity equipamentoexercicio) {
		getEquipamentoexercicios().add(equipamentoexercicio);
		equipamentoexercicio.setExercicio(this);

		return equipamentoexercicio;
	}

	public EquipamentoExercicioEntity removeEquipamentoexercicio(EquipamentoExercicioEntity equipamentoexercicio) {
		getEquipamentoexercicios().remove(equipamentoexercicio);
		equipamentoexercicio.setExercicio(null);

		return equipamentoexercicio;
	}

	public List<ExercicioTreinoEntity> getExerciciotreinos() {
		return this.exerciciotreinos;
	}

	public void setExerciciotreinos(List<ExercicioTreinoEntity> exerciciotreinos) {
		this.exerciciotreinos = exerciciotreinos;
	}

	public ExercicioTreinoEntity addExerciciotreino(ExercicioTreinoEntity exerciciotreino) {
		getExerciciotreinos().add(exerciciotreino);
		exerciciotreino.setExercicio(this);

		return exerciciotreino;
	}

	public ExercicioTreinoEntity removeExerciciotreino(ExercicioTreinoEntity exerciciotreino) {
		getExerciciotreinos().remove(exerciciotreino);
		exerciciotreino.setExercicio(null);

		return exerciciotreino;
	}

	@Override
	protected Integer doGetId() {
		return this.exeid;
	}

}