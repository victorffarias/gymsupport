package br.com.gymsupport.entidades;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * The persistent class for the uf database table.
 * 
 */
@Entity
@Table(name="gymsupport.uf")
@NamedQuery(name="UfEntity.findAll", query="SELECT u FROM UfEntity u")
public class UfEntity extends GSAbstractEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer ufid;

	private String ufdescricao;

	//bi-directional one-to-one association to EnderecoEntity
	@OneToOne(mappedBy="uf")
	private EnderecoEntity endereco;

	public UfEntity() {
	}

	public Integer getUfid() {
		return this.ufid;
	}

	public void setUfid(Integer ufid) {
		this.ufid = ufid;
	}

	public String getUfdescricao() {
		return this.ufdescricao;
	}

	public void setUfdescricao(String ufdescricao) {
		this.ufdescricao = ufdescricao;
	}

	public EnderecoEntity getEndereco() {
		return this.endereco;
	}

	public void setEndereco(EnderecoEntity endereco) {
		this.endereco = endereco;
	}

	@Override
	protected Integer doGetId() {
		return this.ufid;
	}

}