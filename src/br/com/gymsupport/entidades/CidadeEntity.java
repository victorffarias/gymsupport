package br.com.gymsupport.entidades;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * The persistent class for the cidade database table.
 * 
 */
@Entity
@Table(name="gymsupport.cidade")
@NamedQuery(name="CidadeEntity.findAll", query="SELECT c FROM CidadeEntity c")
public class CidadeEntity extends GSAbstractEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer cidid;

	private String ciddescricao;

	//bi-directional one-to-one association to EnderecoEntity
	@OneToOne(mappedBy="cidade")
	private EnderecoEntity endereco;

	public CidadeEntity() {
	}

	public Integer getCidid() {
		return this.cidid;
	}

	public void setCidid(Integer cidid) {
		this.cidid = cidid;
	}

	public String getCiddescricao() {
		return this.ciddescricao;
	}

	public void setCiddescricao(String ciddescricao) {
		this.ciddescricao = ciddescricao;
	}

	public EnderecoEntity getEndereco() {
		return this.endereco;
	}

	public void setEndereco(EnderecoEntity endereco) {
		this.endereco = endereco;
	}

	@Override
	protected Integer doGetId() {
		return this.cidid;
	}

}