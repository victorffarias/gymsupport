package br.com.gymsupport.entidades;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the exerciciotreino database table.
 * 
 */
@Entity
@Table(name="gymsupport.exerciciotreino")
@NamedQuery(name="ExercicioTreinoEntity.findAll", query="SELECT e FROM ExercicioTreinoEntity e")
public class ExercicioTreinoEntity extends GSAbstractEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer exetid;

	private Integer exetdescanso;

	private Integer exetdescansobreakset;

	private Integer exetfatorbreakset;

	private Integer exetfatordropset;

	private Boolean exetflgbreakset;

	private Boolean exetflgdropset;

	private Integer exetnumerorep;

	private String exetobservacao;

	//bi-directional many-to-one association to ExercicioEntity
	@ManyToOne
	@JoinColumn(name="exeid")
	private ExercicioEntity exercicio;

	//bi-directional many-to-one association to TreinoEntity
	@ManyToOne
	@JoinColumn(name="treid")
	private TreinoEntity treino;

	public ExercicioTreinoEntity() {
	}

	public Integer getExetid() {
		return this.exetid;
	}

	public void setExetid(Integer exetid) {
		this.exetid = exetid;
	}

	public Integer getExetdescanso() {
		return this.exetdescanso;
	}

	public void setExetdescanso(Integer exetdescanso) {
		this.exetdescanso = exetdescanso;
	}

	public Integer getExetdescansobreakset() {
		return this.exetdescansobreakset;
	}

	public void setExetdescansobreakset(Integer exetdescansobreakset) {
		this.exetdescansobreakset = exetdescansobreakset;
	}

	public Integer getExetfatorbreakset() {
		return this.exetfatorbreakset;
	}

	public void setExetfatorbreakset(Integer exetfatorbreakset) {
		this.exetfatorbreakset = exetfatorbreakset;
	}

	public Integer getExetfatordropset() {
		return this.exetfatordropset;
	}

	public void setExetfatordropset(Integer exetfatordropset) {
		this.exetfatordropset = exetfatordropset;
	}

	public Boolean getExetflgbreakset() {
		return this.exetflgbreakset;
	}

	public void setExetflgbreakset(Boolean exetflgbreakset) {
		this.exetflgbreakset = exetflgbreakset;
	}

	public Boolean getExetflgdropset() {
		return this.exetflgdropset;
	}

	public void setExetflgdropset(Boolean exetflgdropset) {
		this.exetflgdropset = exetflgdropset;
	}

	public Integer getExetnumerorep() {
		return this.exetnumerorep;
	}

	public void setExetnumerorep(Integer exetnumerorep) {
		this.exetnumerorep = exetnumerorep;
	}

	public String getExetobservacao() {
		return this.exetobservacao;
	}

	public void setExetobservacao(String exetobservacao) {
		this.exetobservacao = exetobservacao;
	}

	public ExercicioEntity getExercicio() {
		return this.exercicio;
	}

	public void setExercicio(ExercicioEntity exercicio) {
		this.exercicio = exercicio;
	}

	public TreinoEntity getTreino() {
		return this.treino;
	}

	public void setTreino(TreinoEntity treino) {
		this.treino = treino;
	}

	@Override
	protected Integer doGetId() {
		return this.exetid;
	}

}