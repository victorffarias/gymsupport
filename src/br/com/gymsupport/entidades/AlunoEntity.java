package br.com.gymsupport.entidades;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * The persistent class for the aluno database table.
 * 
 */
@Entity
@Table(name="gymsupport.aluno")
@NamedQuery(name="AlunoEntity.findAll", query="SELECT a FROM AlunoEntity a")
public class AlunoEntity extends GSAbstractEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer aluid;

	private Integer pesid;

	//bi-directional many-to-one association to AvaliacaoFisicaEntity
	@OneToMany(mappedBy="aluno")
	private List<AvaliacaoFisicaEntity> avaliacaofisicas;

	//bi-directional many-to-one association to TreinoEntity
	@OneToMany(mappedBy="aluno")
	private List<TreinoEntity> treinos;

	//bi-directional one-to-one association to PessoaEntity
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="aluid", insertable = true, updatable = true)
	private PessoaEntity pessoa;

	public AlunoEntity() {
	}

	public Integer getAluid() {
		return this.aluid;
	}

	public void setAluid(Integer aluid) {
		this.aluid = aluid;
	}

	public Integer getPesid() {
		return this.pesid;
	}

	public void setPesid(Integer pesid) {
		this.pesid = pesid;
	}

	public List<AvaliacaoFisicaEntity> getAvaliacaofisicas() {
		return this.avaliacaofisicas;
	}

	public void setAvaliacaofisicas(List<AvaliacaoFisicaEntity> avaliacaofisicas) {
		this.avaliacaofisicas = avaliacaofisicas;
	}

	public AvaliacaoFisicaEntity addAvaliacaofisica(AvaliacaoFisicaEntity avaliacaofisica) {
		getAvaliacaofisicas().add(avaliacaofisica);
		avaliacaofisica.setAluno(this);

		return avaliacaofisica;
	}

	public AvaliacaoFisicaEntity removeAvaliacaofisica(AvaliacaoFisicaEntity avaliacaofisica) {
		getAvaliacaofisicas().remove(avaliacaofisica);
		avaliacaofisica.setAluno(null);

		return avaliacaofisica;
	}

	public List<TreinoEntity> getTreinos() {
		return this.treinos;
	}

	public void setTreinos(List<TreinoEntity> treinos) {
		this.treinos = treinos;
	}

	public TreinoEntity addTreino(TreinoEntity treino) {
		getTreinos().add(treino);
		treino.setAluno(this);

		return treino;
	}

	public TreinoEntity removeTreino(TreinoEntity treino) {
		getTreinos().remove(treino);
		treino.setAluno(null);

		return treino;
	}

	public PessoaEntity getPessoa() {
		return this.pessoa;
	}

	public void setPessoa(PessoaEntity pessoa) {
		this.pessoa = pessoa;
	}

	@Override
	protected Integer doGetId() {
		return this.aluid;
	}

}