package br.com.gymsupport.apresentacao;

import br.com.arquitetura.apresentacao.AbstractCRUDController;
import br.com.arquitetura.negocio.facade.interfaces.ICRUDFacade;
import br.com.arquitetura.to.AbstractTO;

public abstract class GSAbstractCRUDController<T extends ICRUDFacade, I extends AbstractTO> extends AbstractCRUDController<T, I> {

}
