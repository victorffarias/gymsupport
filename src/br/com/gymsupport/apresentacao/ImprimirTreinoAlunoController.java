package br.com.gymsupport.apresentacao;

import java.util.Date;

import br.com.arquitetura.apresentacao.AbstractRelatorioController;
import br.com.gymsupport.negocio.facade.ImprimirTreinoAlunoFacade;
import br.com.gymsupport.negocio.facade.factory.GSFacadeFactory;
import br.com.gymsupport.to.AlunoTO;
import br.com.gymsupport.to.PessoaTO;
import br.com.gymsupport.to.TreinoTO;
import br.com.gymsupport.to.factory.GSTOFactory;

public class ImprimirTreinoAlunoController extends AbstractRelatorioController<ImprimirTreinoAlunoFacade, TreinoTO> {

	@Override
	protected TreinoTO doGetTO() {
		return GSTOFactory.getInstance().createTreinoTO();
	}

	@Override
	protected ImprimirTreinoAlunoFacade doGetFacade() {
		return GSFacadeFactory.getInstance().createImprimirTreinoAlunoFacade();
	}

	@Override
	protected void doPopulaTO(TreinoTO treinoTO) {
		treinoTO.setTreidata(new Date());
		treinoTO.setTreiduracao(30);
		treinoTO.setTreiobjetivo("TESTE");
		AlunoTO alunoTO = GSTOFactory.getInstance().createAlunoTO();
		alunoTO.setAluid(1);
		alunoTO.setAlustatus("A");
		PessoaTO pessoaTO = GSTOFactory.getInstance().createPessoaTO();
		pessoaTO.setPesid(1);
		pessoaTO.setPesnome("Teste");
		pessoaTO.setPesstatus("A");
		pessoaTO.setPesemail("teste@email.com");
		alunoTO.setPessoa(pessoaTO);
		treinoTO.setAluno(alunoTO);
	}

}
