package br.com.gymsupport.apresentacao;

import java.util.Date;

import br.com.gymsupport.negocio.facade.ManterTreinoAlunoFacade;
import br.com.gymsupport.negocio.facade.factory.GSFacadeFactory;
import br.com.gymsupport.to.AlunoTO;
import br.com.gymsupport.to.PessoaTO;
import br.com.gymsupport.to.TreinoTO;
import br.com.gymsupport.to.factory.GSTOFactory;

public class ManterTreinoAlunoController extends GSAbstractCRUDController<ManterTreinoAlunoFacade, TreinoTO> {

	@Override
	protected TreinoTO doGetTO() {
		return GSTOFactory.getInstance().createTreinoTO();
	}

	@Override
	protected ManterTreinoAlunoFacade doGetFacade() {
		return GSFacadeFactory.getInstance().createManterTreinoAlunoFacade();
	}

	@Override
	protected void doPopulaTO(TreinoTO treinoTO) {
		AlunoTO alunoTO = GSTOFactory.getInstance().createAlunoTO();
		alunoTO.setAlustatus("A");
		PessoaTO pessoaTO = GSTOFactory.getInstance().createPessoaTO();
		pessoaTO.setPesnome("Teste");
		pessoaTO.setPesstatus("A");
		pessoaTO.setPesemail("teste@email.com");
		alunoTO.setPessoa(pessoaTO);
		treinoTO.setAluno(alunoTO);
		treinoTO.setTreidata(new Date());
		treinoTO.setTreiduracao(30);
		treinoTO.setTreiobjetivo("Teste");
	}

	@Override
	protected void doPopulaTO(TreinoTO to, TreinoTO toBanco) {
		this.doPopulaTO(to);
		to.setTreid(toBanco.getTreid());
	}

}
