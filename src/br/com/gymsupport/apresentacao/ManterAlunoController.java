package br.com.gymsupport.apresentacao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Timestamp;

import br.com.arquitetura.to.AbstractTOFile;
import br.com.gymsupport.negocio.facade.ManterAlunoFacade;
import br.com.gymsupport.negocio.facade.factory.GSFacadeFactory;
import br.com.gymsupport.to.AlunoTO;
import br.com.gymsupport.to.PessoaTO;
import br.com.gymsupport.to.factory.GSTOFactory;

public class ManterAlunoController extends GSAbstractCRUDController<ManterAlunoFacade, AlunoTO> {

	@Override
	protected AlunoTO doGetTO() {
		return GSTOFactory.getInstance().createAlunoTO();
	}

	@Override
	protected ManterAlunoFacade doGetFacade() {
		return GSFacadeFactory.getInstance().createManterAlunoFacade();
	}

	@Override
	protected void doPopulaTO(AlunoTO alunoTO) {
		alunoTO.setAlustatus("A");
		PessoaTO pessoaTO = GSTOFactory.getInstance().createPessoaTO();
		pessoaTO.setPesnome("Teste");
		pessoaTO.setPesstatus("A");
		pessoaTO.setPesemail("teste@email.com");
		alunoTO.setPessoa(pessoaTO);
		
		AbstractTOFile toFile = GSTOFactory.getInstance().createAbstractTOFile();
		
		File file = new File("test", "arquivo.txt");
		InputStream is = null;
		try {
			is = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		toFile.setFile(is);
		toFile.setArqExtensao("txt");
		toFile.setArqNome("arquivo");
		toFile.setArqData(new Timestamp(0));		
		toFile.setArqStatus(true);
		toFile.setArqTamanho((long) 100);
		
		alunoTO.getPessoa().setArquivo(toFile);
	}

	@Override
	protected void doPopulaTO(AlunoTO to, AlunoTO toBanco) {
		this.doPopulaTO(to);
		to.setAluid(toBanco.getAluid());
	}

}
