package br.com.gymsupport.negocio.model;

import br.com.arquitetura.integracao.AbstractDAO;
import br.com.gymsupport.entidades.AlunoEntity;
import br.com.gymsupport.entidades.PessoaEntity;
import br.com.gymsupport.entidades.TreinoEntity;
import br.com.gymsupport.entidades.factory.GSEntityFactory;
import br.com.gymsupport.integracao.factory.GSDAOFactory;
import br.com.gymsupport.to.AlunoTO;
import br.com.gymsupport.to.PessoaTO;
import br.com.gymsupport.to.TreinoTO;
import br.com.gymsupport.to.factory.GSTOFactory;

public class TreinoModel extends GSAbstractCRUDModel<TreinoTO, TreinoEntity> {

	@Override
	protected void doPopulaEntity(TreinoEntity treinoEntity, TreinoTO to) {
		PessoaEntity pessoaEntity = GSEntityFactory.getInstance().createPessoaEntity();
		pessoaEntity.setPesnome(to.getAluno().getPessoa().getPesnome());
		pessoaEntity.setPesemail(to.getAluno().getPessoa().getPesemail());
		AlunoEntity alunoEntity = GSEntityFactory.getInstance().createAlunoEntity();
		alunoEntity.setPessoa(pessoaEntity);
		
		treinoEntity.setTreid(to.getTreid());
		treinoEntity.setAluno(alunoEntity);
		treinoEntity.setTreidata(to.getTreidata());
		treinoEntity.setTreiduracao(to.getTreiduracao());
		treinoEntity.setTreiobjetivo(to.getTreiobjetivo());
	}

	@Override
	protected TreinoTO doPopulaTO(TreinoTO treinoTO, TreinoEntity treinoEntity) {
		AlunoTO alunoTO = GSTOFactory.getInstance().createAlunoTO();
		PessoaTO pessoaTO = GSTOFactory.getInstance().createPessoaTO();
		PessoaEntity pessoaEntity = treinoEntity.getAluno().getPessoa();
		pessoaTO.setPesnome(pessoaEntity.getPesnome());
		pessoaTO.setPesemail(pessoaEntity.getPesemail());
		alunoTO.setPessoa(pessoaTO);
		treinoTO.setAluno(alunoTO);
		treinoTO.setTreid(treinoEntity.getTreid());
		treinoTO.setTreidata(treinoEntity.getTreidata());
		treinoTO.setTreiduracao(treinoEntity.getTreiduracao());
		treinoTO.setTreiobjetivo(treinoEntity.getTreiobjetivo());
		
		return treinoTO;
	}

	@Override
	protected TreinoEntity doCreateEntity() {
		return GSEntityFactory.getInstance().createTreinoEntity();
	}

	@Override
	protected TreinoTO doCreateTO() {
		return GSTOFactory.getInstance().createTreinoTO();
	}

	@Override
	protected AbstractDAO<TreinoEntity> doGetDAO() {
		return GSDAOFactory.getInstance().createTreinoDAO();
	}

	@Override
	protected void doAfterSave(TreinoEntity entity, TreinoTO to) {
		// TODO Auto-generated method stub
		
	}

	
}
