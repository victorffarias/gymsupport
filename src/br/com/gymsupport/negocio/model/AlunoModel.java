package br.com.gymsupport.negocio.model;

import br.com.arquitetura.entidades.AbstractEntityFile;
import br.com.arquitetura.integracao.AbstractDAO;
import br.com.gymsupport.entidades.AlunoEntity;
import br.com.gymsupport.entidades.PessoaEntity;
import br.com.gymsupport.entidades.factory.GSEntityFactory;
import br.com.gymsupport.integracao.factory.GSDAOFactory;
import br.com.gymsupport.to.AlunoTO;
import br.com.gymsupport.to.PessoaTO;
import br.com.gymsupport.to.factory.GSTOFactory;

public class AlunoModel extends GSAbstractCRUDModel<AlunoTO, AlunoEntity> {

	@Override
	protected void doPopulaEntity(AlunoEntity alunoEntity, AlunoTO to) {
		GSEntityFactory factory = GSEntityFactory.getInstance();
		alunoEntity.setAluid(to.getAluid());
		PessoaEntity pessoaEntity = factory.createPessoaEntity();
		pessoaEntity.setPesid(to.getPessoa().getPesid());
		pessoaEntity.setPesnome(to.getPessoa().getPesnome());
		pessoaEntity.setPesemail(to.getPessoa().getPesemail());
		alunoEntity.setPessoa(pessoaEntity);
		
		AbstractEntityFile entityFile = GSEntityFactory.getInstance().createAbstractEntityFile();
		
		entityFile.setInputStream(to.getPessoa().getArquivo().getFile());
		entityFile.setArqextensao(to.getPessoa().getArquivo().getArqExtensao());
		entityFile.setArqnome(to.getPessoa().getArquivo().getArqNome());
		entityFile.setArqdata(to.getPessoa().getArquivo().getArqData());
		entityFile.setArqstatus(to.getPessoa().getArquivo().getArqStatus());
		entityFile.setArqtamanho(to.getPessoa().getArquivo().getArqTamanho());
		
		alunoEntity.getPessoa().setArquivo(entityFile);
	}

	@Override
	protected AlunoTO doPopulaTO(AlunoTO alunoTO, AlunoEntity alunoEntity) {
		GSTOFactory gstoFactory = GSTOFactory.getInstance();
		alunoTO.setAluid(alunoEntity.getAluid());
		PessoaTO pessoaTO = gstoFactory.createPessoaTO();
		pessoaTO.setPesid(alunoEntity.getPessoa().getPesid());
		pessoaTO.setPesnome(alunoEntity.getPessoa().getPesnome());
		pessoaTO.setPesemail(alunoEntity.getPessoa().getPesemail());
		alunoTO.setPessoa(pessoaTO);
		
		return alunoTO;
	}

	@Override
	protected AlunoEntity doCreateEntity() {
		return GSEntityFactory.getInstance().createAlunoEntity();
	}

	@Override
	protected AlunoTO doCreateTO() {
		return GSTOFactory.getInstance().createAlunoTO();
	}

	@Override
	protected AbstractDAO<AlunoEntity> doGetDAO() {
		return GSDAOFactory.getInstance().createAlunoDAO();
	}

	@Override
	protected void doAfterSave(AlunoEntity entity, AlunoTO to) {
		this.save(to.getPessoa().getArquivo(), entity.getPessoa().getArquivo());
	}

	
}
