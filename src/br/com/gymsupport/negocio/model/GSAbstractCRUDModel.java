package br.com.gymsupport.negocio.model;

import br.com.arquitetura.entidades.AbstractEntity;
import br.com.arquitetura.negocio.model.AbstractCRUDModel;
import br.com.arquitetura.to.AbstractTO;

public abstract class GSAbstractCRUDModel<T extends AbstractTO,I extends AbstractEntity> extends AbstractCRUDModel<T, I> {
}
