package br.com.gymsupport.negocio.model.factory;

import br.com.arquitetura.negocio.model.factory.ModelFactory;
import br.com.gymsupport.negocio.model.AlunoModel;
import br.com.gymsupport.negocio.model.RelatorioTreinoModel;
import br.com.gymsupport.negocio.model.TreinoModel;

public class GSModelFactory extends ModelFactory {
	static GSModelFactory instance = null;
	
	public static GSModelFactory getInstance(){
		if (instance == null) {
			instance = new GSModelFactory();
		}
		return instance;
	}
	
	public AlunoModel createAlunoModel(){
		return new AlunoModel();
	}
	
	public TreinoModel createTreinoModel(){
		return new TreinoModel();
	}
	
	public RelatorioTreinoModel createRelatorioTreinoModel(){
		return new RelatorioTreinoModel();
	}
}
