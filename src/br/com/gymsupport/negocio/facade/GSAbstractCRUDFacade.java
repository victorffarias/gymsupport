package br.com.gymsupport.negocio.facade;

import br.com.arquitetura.negocio.facade.AbstractCRUDFacade;
import br.com.arquitetura.negocio.model.interfaces.ICRUDModel;
import br.com.arquitetura.to.AbstractTO;

public abstract class GSAbstractCRUDFacade<T extends ICRUDModel, I extends AbstractTO> extends AbstractCRUDFacade<T, I> {
}
