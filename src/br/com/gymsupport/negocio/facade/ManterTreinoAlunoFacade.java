package br.com.gymsupport.negocio.facade;

import br.com.gymsupport.negocio.model.TreinoModel;
import br.com.gymsupport.negocio.model.factory.GSModelFactory;
import br.com.gymsupport.to.TreinoTO;

public class ManterTreinoAlunoFacade  extends GSAbstractCRUDFacade<TreinoModel, TreinoTO> {

	protected TreinoModel doGetModel() {
		return GSModelFactory.getInstance().createTreinoModel();
	}

}
