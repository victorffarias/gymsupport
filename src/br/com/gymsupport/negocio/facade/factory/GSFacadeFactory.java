package br.com.gymsupport.negocio.facade.factory;

import br.com.arquitetura.negocio.facade.factory.FacadeFactory;
import br.com.gymsupport.negocio.facade.ImprimirTreinoAlunoFacade;
import br.com.gymsupport.negocio.facade.ManterAlunoFacade;
import br.com.gymsupport.negocio.facade.ManterTreinoAlunoFacade;

public class GSFacadeFactory extends FacadeFactory {
	static GSFacadeFactory instance = null;
	
	public static GSFacadeFactory getInstance(){
		if (instance == null) {
			instance = new GSFacadeFactory();
		}
		
		return instance;
	}
	
	public ManterAlunoFacade createManterAlunoFacade(){
		return new ManterAlunoFacade();
	}
	
	public ManterTreinoAlunoFacade createManterTreinoAlunoFacade(){
		return new ManterTreinoAlunoFacade();
	}
	
	public ImprimirTreinoAlunoFacade createImprimirTreinoAlunoFacade(){
		return new ImprimirTreinoAlunoFacade();
	}
}
