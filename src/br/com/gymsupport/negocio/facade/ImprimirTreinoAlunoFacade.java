package br.com.gymsupport.negocio.facade;

import br.com.arquitetura.negocio.facade.AbstractRelatorioFacade;
import br.com.gymsupport.negocio.model.RelatorioTreinoModel;
import br.com.gymsupport.negocio.model.factory.GSModelFactory;
import br.com.gymsupport.to.TreinoTO;

public class ImprimirTreinoAlunoFacade extends AbstractRelatorioFacade<RelatorioTreinoModel, TreinoTO> {

	protected RelatorioTreinoModel doGetModel() {
		return GSModelFactory.getInstance().createRelatorioTreinoModel();
	}

}
