package br.com.gymsupport.negocio.facade;

import br.com.gymsupport.negocio.model.AlunoModel;
import br.com.gymsupport.negocio.model.factory.GSModelFactory;
import br.com.gymsupport.to.AlunoTO;

public class ManterAlunoFacade extends GSAbstractCRUDFacade<AlunoModel, AlunoTO> {

	protected AlunoModel doGetModel() {
		return GSModelFactory.getInstance().createAlunoModel();
	}

}
