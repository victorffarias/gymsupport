package br.com.gymsupport.integracao.factory;

import br.com.arquitetura.integracao.factory.DAOFactory;
import br.com.gymsupport.integracao.AlunoDAO;
import br.com.gymsupport.integracao.TreinoDAO;

public class GSDAOFactory extends DAOFactory {
	static GSDAOFactory instance = null;
	
	public static GSDAOFactory getInstance(){
		if (instance == null) {
			instance = new GSDAOFactory();
		}
		
		return instance;
	}
	
	public AlunoDAO createAlunoDAO(){
		return new AlunoDAO();
	}
	
	public TreinoDAO createTreinoDAO(){
		return new TreinoDAO();
	}
}
