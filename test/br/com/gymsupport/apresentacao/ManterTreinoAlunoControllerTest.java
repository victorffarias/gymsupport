package br.com.gymsupport.apresentacao;

import java.util.ArrayList;

import junit.framework.TestCase;
import br.com.gymsupport.to.AlunoTO;
import br.com.gymsupport.to.TreinoTO;

public class ManterTreinoAlunoControllerTest extends TestCase {

	public void testSalvar() {
		ManterTreinoAlunoController controller = new ManterTreinoAlunoController();
		controller.save();
	}
	
	public void testList(){
		ManterTreinoAlunoController controller = new ManterTreinoAlunoController();
		controller.save();
		ArrayList<TreinoTO> treinoTOs = (ArrayList<TreinoTO>) controller.list();
		assertNotNull(treinoTOs);
	}
	
	public void testDelete(){
		ManterTreinoAlunoController controller = new ManterTreinoAlunoController();
		assertEquals("Ok", controller.delete());
	}
	
	public void testRead(){
		ManterTreinoAlunoController controller = new ManterTreinoAlunoController();
		controller.save();
		ArrayList<TreinoTO> treinoTOs = (ArrayList<TreinoTO>) controller.list();
		TreinoTO aluno = controller.read(treinoTOs.get(0));
		assertNotNull(aluno);
	}
	
	
}
