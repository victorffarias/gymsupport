package br.com.gymsupport.apresentacao;

import java.util.ArrayList;

import junit.framework.TestCase;
import br.com.gymsupport.to.AlunoTO;

public class ManterAlunoControllerTest extends TestCase {

	public void testSalvar() {
		ManterAlunoController alunoController = new ManterAlunoController();
		assertEquals("OK", alunoController.save());
	}
	
	public void testList(){
		ManterAlunoController alunoController = new ManterAlunoController();
		alunoController.save();
		ArrayList<AlunoTO> alunoTOs = (ArrayList<AlunoTO>) alunoController.list();
		assertNotNull(alunoTOs);
	}
	
	public void testDelete(){
		ManterAlunoController alunoController = new ManterAlunoController();
		assertEquals("Ok", alunoController.delete());
	}
	
	public void testRead(){
		ManterAlunoController alunoController = new ManterAlunoController();
		alunoController.save();
		ArrayList<AlunoTO> alunoTOs = (ArrayList<AlunoTO>) alunoController.list();
		AlunoTO aluno = alunoController.read(alunoTOs.get(0));
		assertNotNull(aluno);
	}
}